Description
----------
Simple file storage api.
Availible routes

``POST   /api/v1/files``               - upload file;

``GET    /api/v1/file/{filename}`` - download file;

``DELETE /api/v1/file/{filename}`` - delete file.

Installation
----------

Clone project and install project dependencies.

    git clone https://gitlab.com/kozhemiakink/simple-file-storage.git
    cd simple-file-storage
    pip3 install -r requirements.txt

Before running project, set settings through enviroment variables

    export SECRET_KEY='app secret key'
    export STORAGE_PATH=$PWD/store

Than run project
- with flask
    ```
    export FLASK_APP="fstorage/app:create_app('fstorage.config.DevelopementConfig')"
    flask run
    ```
- with gunicorn
    ```
    pip3 install wheel==0.36.2
    pip3 install gunicorn==20.1.0
    gunicorn -b 0.0.0.0:8080 "fstorage.app:create_app('fstorage.config.ProductionConfig')" --daemon
    ```
Settings
----------
- ``SECRET_KEY``         - web application secret key;
- ``STORAGE_PATH``       - file storage directory. Application root directory by default;
- ``TMP_STORAGE_PATH``   - temporary storage directory for downloading files. STORAGE_PATH/tmp by default;
- ``MAX_CONTENT_LENGTH`` - max file size in bytes. 20M by default.

Usage example

    export SECRET_KEY='your very secret app key'
    export STORAGE_PATH=$PWD/store
    export TMP_STORAGE_PATH=/tmp/store
    export MAX_CONTENT_LENGTH=$((64 * 1024 * 1024)) # 64M

Curl testing
-------------

Making some testing files

    dd if=/dev/urandom of=file1 bs=1M count=8
    touch emptyfile

Upload files to the storage with curl

    curl -X POST -F 'data=@file1' http://localhost:8080/api/v1/files
    curl -X POST -F 'data=@emptyfile' http://localhost:8080/api/v1/files

Download files (use your own hashes for files)

    curl -o file1_test http://localhost:8080/api/v1/file/4b7412bcd4fa27fa69d4b2d36a72512cc9a8c1e69128097d1105a19d51ea886b
    curl -o emptyfile_test http://localhost:8080/api/v1/file/e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855

Check if uploaded and downloaded files are the same

    cmp --silent file1 file1_test && echo 'same files' || echo 'different files'
    cmp --silent emptyfile emptyfile_test && echo 'same files' || echo 'different files'

Delete files from storage (use your own hashes for files)

    curl -X DELETE http://localhost:8080/api/v1/file/4b7412bcd4fa27fa69d4b2d36a72512cc9a8c1e69128097d1105a19d51ea886b
    curl -X DELETE http://localhost:8080/api/v1/file/e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855

Check if files realy deleted from storage (must be 404)

    curl -I http://localhost:8080/api/v1/file/4b7412bcd4fa27fa69d4b2d36a72512cc9a8c1e69128097d1105a19d51ea886b
    curl -I http://localhost:8080/api/v1/file/e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855


