import os


MB = 1024 * 1024

class BaseConfig:
    ROOT_PATH = os.path.abspath(os.path.dirname(__file__))

    SECRET_KEY = os.environ.get('SECRET_KEY', 'A SECRET KEY')
    # path to the storage directory (absolute path recommended)
    STORAGE_PATH = os.environ.get('STORAGE_PATH', 'store/')
    if os.path.abspath(STORAGE_PATH):
        STORAGE_PATH = os.path.join(ROOT_PATH, STORAGE_PATH)

    # path to the tmp storage directory (absolute path recommended)
    TMP_STORAGE_PATH = os.environ.get(
        'TMP_STORAGE_PATH',
        os.path.join(STORAGE_PATH, 'tmp')
    )
    # max file size
    MAX_CONTENT_LENGTH = int(os.environ.get('MAX_CONTENT_LENGTH', 20 * MB))


class DevelopementConfig(BaseConfig):
    DEBUG = True


class ProductionConfig(BaseConfig):
    DEBUG = False
