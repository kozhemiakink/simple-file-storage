import os
import re
import shutil
import hashlib
from flask import (Blueprint, current_app,
    jsonify, request, send_file)


blueprint = Blueprint('file', __name__)


@blueprint.route('/api/v1/files', methods=('POST',))
def upload_file():
    files = tuple(request.files.keys())
    if len(files) != 1:
        return jsonify(err_message='Only one file supported'), 422

    file_storage = request.files[files[0]]
    algo = hashlib.sha256()
    tmp_filename = hashlib.sha1(os.urandom(128)).hexdigest()
    tmp_filepath = os.path.join(
        current_app.config['TMP_STORAGE_PATH'],
        tmp_filename
    )
    # this cycle needs to avoid memory leak
    # instead of "data = file_storage.read(current_app.config['MAX_CONTENT_LENGTH'])"
    with open(tmp_filepath, 'wb') as file:
        while True:
            data = file_storage.read(65536) # 64 KB
            if len(data) == 0:
                break
            algo.update(data)
            file.write(data)

    filename = algo.hexdigest()
    base_filepath = os.path.join(
        current_app.config['STORAGE_PATH'],
        filename[:2],
    )
    filepath = os.path.join(base_filepath, filename)

    if not os.path.exists(base_filepath):
        os.makedirs(base_filepath)

    shutil.move(tmp_filepath, filepath)

    return jsonify(filename=filename), 201


@blueprint.route('/api/v1/file/<string:filename>', methods=('GET',))
def download_file(filename):
    # filename must be safe (it must be sha256 hash)
    if re.match(r'^[0-9a-f]{64}$', filename) is None:
        return jsonify(err_message='Bad filename'), 422

    filepath = os.path.join(
        current_app.config['STORAGE_PATH'],
        filename[:2],
        filename
    )
    # there can be a file integrity check
    # file content hash and filename must be equal
    if (not os.access(filepath, os.R_OK) or
        not os.path.exists(filepath) or
        not os.path.isfile(filepath)):
        return jsonify(err_message='File not found'), 404

    return send_file(
        filepath,
        as_attachment=True,
        attachment_filename=filename
    )


@blueprint.route('/api/v1/file/<string:filename>', methods=('DELETE',))
def delete_file(filename):
    # filename must be safe (it must be sha256 hash)
    if re.match(r'^[0-9a-f]{64}$', filename) is None:
        return jsonify(err_message='Bad filename'), 422

    filepath = os.path.join(
        current_app.config['STORAGE_PATH'],
        filename[:2],
        filename
    )
    if (os.access(filepath, os.W_OK) and
        os.path.isfile(filepath)):
        os.remove(filepath)

    return '', 204
