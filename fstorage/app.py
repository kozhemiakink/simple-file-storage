import os
from flask import Flask

from fstorage import file


def create_app(config_path):
    app = Flask(__name__)
    app.config.from_object(config_path)

    storage_path = app.config['STORAGE_PATH']

    if os.path.isfile(storage_path):
        raise OSError(f'{storage_path} is an existing file')

    if not os.path.exists(storage_path):
        os.makedirs(storage_path)

    tmp_storage_path = app.config['TMP_STORAGE_PATH']
    if os.path.isfile(tmp_storage_path):
        raise OSError(f'{tmp_storage_path} is an existing file')

    if not os.path.exists(tmp_storage_path):
        os.makedirs(tmp_storage_path)

    register_shellcontext(app)
    register_blueprints(app)
    register_commands(app)
    return app


def register_blueprints(app):
    app.register_blueprint(file.views.blueprint)


def register_shellcontext(app):
    def shell_context():
        return {}

    app.shell_context_processor(shell_context)


def register_commands(app):
    pass
